import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

ReactDOM.render(
  <Routes basename={process.env.REACT_APP_HOME_URL || process.env.PUBLIC_URL}/>,
  document.getElementById('root')
);
