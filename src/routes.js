import React from 'react';
import {Redirect, BrowserRouter, Route, Switch} from 'react-router-dom'
import {NotFoundContainer, DirectoryContainer, DetailsContainer} from './containers';

const Routes = (props) => (
  <BrowserRouter {...props}>
    <Switch>
      <Route exact path="/" component={DirectoryContainer}/>
      <Route path="/Details/:id" component={DetailsContainer}/>
      <Route path="/NotFound" component={NotFoundContainer}/>
      <Redirect from="*" to="/NotFound"/>
    </Switch>
  </BrowserRouter>
);

export default Routes;