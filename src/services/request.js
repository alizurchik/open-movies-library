export function request(url, options) {
  return fetch(url, options)
    .then(checkStatus);
}

export function buildQueryString(params) {
  if (!params || typeof params !== 'object') {
    return '';
  }

  const keys = Object.keys(params);

  if (keys.length === 0) {
    return '';
  }

  return '?' + keys
      // exclude empty values
      .filter(key => !!params[key])
      .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
      .join('&');
}

function checkStatus(response) {
  if (response.ok) {
    return response;
  }

  const error = new Error(response.statusText);

  error.response = response;

  throw error;
}
