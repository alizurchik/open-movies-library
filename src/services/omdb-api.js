import {request, buildQueryString} from './request';
import {capitalizeWord} from '../core/utils';

const apiUrl = 'http://www.omdbapi.com/';
export const ITEMS_PER_PAGE = 10;

export class OmdbApi {
  constructor(options) {
    this.defaults = {
      cache: false // TODO
    };
    this.options = Object.assign({...this.defaults}, options);
  }

  byId(imdbId) {
    return makeApiCall(apiUrl, {i: imdbId})
      .then(remapMovie);
  }

  byTitle(title) {
    return makeApiCall(apiUrl, {t: title})
      .then(remapMovie);
  }

  search({query, page, year, type}) {
    return makeApiCall(apiUrl, {s: query, page, y: year, type})
      .then(mapResult)
      .then(removeTwinsFromResult);
  }
}

export const omdbApi = new OmdbApi();
export const getInstance = options => new OmdbApi(options);

function makeApiCall(url, queryObj) {
  return request(url + buildQueryString(queryObj))
    .then(resp => resp.json())
    .then(checkStatus);
}

function checkStatus(response) {
  if (response.Error) {
    throw new Error(response.Error);
  }

  return response;
}

function remapMovie(movie) {
  return {
    actors: movie.Actors,
    id: movie.imdbID,
    plot: movie.Plot,
    poster: movie.Poster,
    rating: movie.imdbRating,
    title: movie.Title,
    type: capitalizeWord(movie.Type),
    year: movie.Year
  };
}

function mapResult(result) {
  return {
    items: result.Search.map(remapMovie),
    total: parseInt(result.totalResults, 10)
  };
}

/**
 * Sometimes API returns the same movies twice. This method removes copies *
 * @param result
 */
function removeTwinsFromResult(result) {
  if (Array.isArray(result.items)) {
    const ids = [];

    result.items = result.items.filter(({id}) => {
      if (ids.indexOf(id) === -1) {
        ids.push(id);

        return true;
      }

      return false;
    });
  }

  return result;
}
