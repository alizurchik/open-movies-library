import React from 'react';
import {CheckboxMultiple} from '../';
import PropTypes from 'prop-types';

export default class MoviesFilter extends React.Component {
  render() {
    return (
      <section>
        {this.props.years.length > 0 && (
          <p>
            Year: <CheckboxMultiple groupName="years" onChange={this.props.onSelectYear} defaultChoices={this.props.years}/>
          </p>
        )}
        {this.props.types.length > 0 && (
          <p>
            Type: <CheckboxMultiple groupName="types" onChange={this.props.onSelectType} defaultChoices={this.props.types}/>
          </p>
        )}
      </section>
    )
  }
}

MoviesFilter.propTypes = {
  years: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string
  })),
  types: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string
  })),
  onSelectYear: PropTypes.func,
  onSelectType: PropTypes.func
};

MoviesFilter.defaultProps = {
  years: [],
  types: []
};
