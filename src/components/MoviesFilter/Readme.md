Example:

    <MoviesFilter
        onSelectYear={(year, val) => console.log(year, val)}
        onSelectType={(type, val) => console.log(type, val)}
        years={[{name: '1987'}, {name: '1991'}]}
        types={[{name: 'movie'}, {name: 'game'}]}
    />