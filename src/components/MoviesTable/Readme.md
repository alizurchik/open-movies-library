Table with data:

    <MoviesTable movies={[{
        id: 'tt0093870',
        poster: 'https://images-na.ssl-images-amazon.com/images/M/MV5BZWVlYzU2ZjQtZmNkMi00OTc3LTkwZmYtZDVjNmY4OWFmZGJlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
        title: 'Robocop',
        type: 'movie',
        year: '1987',
        rating: '7.5'
    }]} onClick={movie => console.log(movie)} />