import React from 'react';
import {Table, Image} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {isFunction} from '../../core/utils';

export default class MoviesTable extends React.Component {
  handleClick = movie => e => {
    isFunction(this.props.onClick) && this.props.onClick(movie, e);
  };

  renderMovie = movie => {
    return (
      <tr key={movie.id} onClick={this.handleClick(movie)} className="MoviesTable__row">
        <td className="MoviesTable__poster"><Image responsive src={movie.poster} thumbnail /></td>
        <td>{movie.title}</td>
        <td>{movie.type}</td>
        <td>{movie.year}</td>
      </tr>
    );
  };

  render() {
    return (
      <Table responsive className="MoviesTable">
        <thead>
        <tr>
          <th>Poster</th>
          <th>Title</th>
          <th>Type</th>
          <th>Year</th>
        </tr>
        </thead>
        <tbody>
        {this.props.movies.map(this.renderMovie)}
        </tbody>
      </Table>
    );
  }
}

MoviesTable.propTypes = {
  movies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    poster: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.string
  })),
  onClick: PropTypes.func
};

MoviesTable.defaultProps = {
  movies: []
};