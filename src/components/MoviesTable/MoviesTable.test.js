import React from 'react';
import ReactDOM from 'react-dom';
import MoviesTable from './MoviesTable';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MoviesTable />, div);
});
