import NotFound from './NotFound';
import CheckboxMultiple from './CheckboxMultiple';
import SearchBar from './SearchBar';
import MoviesTable from './MoviesTable';
import Paginator from './Paginator';
import MovieDetails from './MovieDetails';
import MoviesFilter from './MoviesFilter';

export {NotFound, CheckboxMultiple, SearchBar, MoviesTable, Paginator, MovieDetails, MoviesFilter};