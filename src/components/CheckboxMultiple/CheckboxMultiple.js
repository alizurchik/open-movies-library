import React from 'react';
import PropTypes from 'prop-types';
import {FormGroup, Radio} from 'react-bootstrap';
import {isFunction} from '../../core/utils';

export default class CheckboxMultiple extends React.Component {
  render() {
    if (this.props.defaultChoices.length === 0) {
      return null
    }

    this.props.defaultChoices.unshift({name: '_all', title: 'All'});

    return (
      <FormGroup>
        {this.props.defaultChoices.map(this.renderRadio)}
      </FormGroup>
    );
  }

  handleChange = choice => e => this.changeValue(choice, e.target.checked);

  changeValue(choice, value) {
    isFunction(this.props.onChange) && this.props.onChange(choice, value);
  }

  renderRadio = choice => (
    <Radio
      name={this.props.groupName}
      onChange={this.handleChange(choice)}
      inline={this.props.inline}
      key={choice.name}>
      {choice.title || choice.name}
    </Radio>
  );
}

CheckboxMultiple.propTypes = {
  defaultChoices: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool
  })),
  inline: PropTypes.bool,
  onChange: PropTypes.func,
  groupName: PropTypes.string
};

CheckboxMultiple.defaultProps = {
  defaultChoices: [],
  inline: true
};
