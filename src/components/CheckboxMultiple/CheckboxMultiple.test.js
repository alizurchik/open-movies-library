import React from 'react';
import ReactDOM from 'react-dom';
import CheckboxMultiple from './CheckboxMultiple';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CheckboxMultiple />, div);
});
