Without choices:

    <CheckboxMultiple />

With choices *(look at console for event handler's result)*:

    <CheckboxMultiple
        onChange={(choice, selected) => console.log(choice, selected)}
        defaultChoices={[{name: 'apple', checked: false}, {name: 'bananna', checked: true, title: 'Bananna'}]}
    />
