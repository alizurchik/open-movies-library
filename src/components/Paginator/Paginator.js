import React from 'react';
import {Pagination} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function Paginator(props) {
  const {
    pages, page, // skip
    ...rest
  } = props;

  return <Pagination
    bsSize="medium"
    prev
    next
    first
    last
    ellipsis
    boundaryLinks
    {...rest}
    items={props.pages}
    activePage={props.page}
    maxButtons={props.maxButtons}
    onSelect={props.onSelect} />;
}

Paginator.propTypes = {
  page: PropTypes.number,
  pages: PropTypes.number,
  maxButtons: PropTypes.number,
  onSelect: PropTypes.func
};

Paginator.defaultProps = {
  page: 1,
  pages: 0,
  maxButtons: 5
};