Simple:

    <SearchBar placeholder="Type something" />

With label:

    <SearchBar label="Type something" />

With default value:

    <SearchBar defaultValue="Type something" />

Listen to `onChange`:

    <SearchBar onChange={val => console.log(val)} />
