import React from 'react';
import PropTypes from 'prop-types';
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap';
import {debounce, isFunction} from '../../core/utils';

export default class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);

    if (this.props.debounce > 0) {
      this.onChangeDebounced = debounce(this.props.onChange, this.props.debounce);
    }
  }

  onChange(e) {
    if (isFunction(this.props.onChange)) {
      const val = (e.target.value || '').trim();

      if (this.props.debounce > 0) {
        this.onChangeDebounced(val);
      } else {
        this.props.onChange(val)
      }
    }
  };

  render() {
    const {
      onChange, // skip
      debounce, // skip
      label,
      ...props
    } = this.props;

    return (
      <FormGroup>
        {label && <ControlLabel>{label}</ControlLabel>}
        <FormControl
          {...props}
          className="SearchBar"
          onChange={this.onChange}
        />
      </FormGroup>
    );
  }
}

SearchBar.propTypes = {
  debounce: PropTypes.number,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  inputRef: PropTypes.func,
  label: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string
};