Example:

    <MovieDetails movie={{
        title: 'RoboCop',
        poster: 'https://images-na.ssl-images-amazon.com/images/M/MV5BZWVlYzU2ZjQtZmNkMi00OTc3LTkwZmYtZDVjNmY4OWFmZGJlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
        plot: 'In a dystopic and crime-ridden Detroit, a terminally wounded cop returns to the force as a powerful cyborg haunted by submerged memories.',
        actors: `Peter Weller, Nancy Allen, Dan O'Herlihy, Ronny Cox`,
        type: 'Movie',
        rating: '7.5',
        year: '1987'
    }} />