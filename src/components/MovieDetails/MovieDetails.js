import React from 'react';
import {Grid, Row, Col, PageHeader, Well, Image, Label} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default class MovieDetails extends React.Component {
  renderActors(actors) {
    if (!Array.isArray(actors)) {
      actors = actors.split(', ');
    }

    return actors.map(actor => (
      <Label key={actor} className="MovieDetails__actor" bsStyle="info">{actor}</Label>
    ));
  }

  render() {
    return (
      <Grid className="MovieDetails">
        <Row>
          <Col>
            <PageHeader>{this.props.movie.title} ({this.props.movie.type})</PageHeader>
          </Col>
        </Row>
        <Row>
          <Col>
            <Well className="clearfix">
              <Row>
                <Col sm={4}>
                  <Image src={this.props.movie.poster} thumbnail/>
                </Col>
                <Col sm={8}>
                  <p className="MovieDetails__plot">{this.props.movie.plot}</p>
                  <p>Actors: {this.renderActors(this.props.movie.actors)}</p>
                </Col>
                <Col sm={12}>
                  <h3>Released in {this.props.movie.year}</h3>
                  <h3>IMDb score: <Label bsStyle="success">{this.props.movie.rating}</Label></h3>
                </Col>
              </Row>
            </Well>
          </Col>
        </Row>
      </Grid>
    );
  }
}

MovieDetails.propTypes = {
  movie: PropTypes.shape({
    actors: PropTypes.string,
    id: PropTypes.string,
    plot: PropTypes.string,
    poster: PropTypes.string,
    rating: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.string,
    year: PropTypes.string
  })
};
