import React from 'react';
import PropTypes from 'prop-types';

export default class NotFound extends React.Component {
  render() {
    return (
      <div className="NotFound">
        <h1>
          Error 404: <small>Page Not Found</small>
        </h1>
        {this.props.homeUrl && (
          <a href={this.props.homeUrl}>Go home</a>
        )}
      </div>
    );
  }
}

NotFound.propTypes = {
  homeUrl: PropTypes.string
};

NotFound.defaultProps = {
  homeUrl: null
};