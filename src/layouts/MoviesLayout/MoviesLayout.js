import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function MoviesLayout({movies, pagination, filter}) {
  return (
    <Grid>
      <Row>
        <Col xs={10} xsOffset={1} md={6} mdOffset={3} className="text-center">
          {pagination}
        </Col>
      </Row>
      {filter}
      {movies}
      <Row>
        <Col xs={10} xsOffset={1} md={6} mdOffset={3} className="text-center">
          {pagination}
        </Col>
      </Row>
    </Grid>
  );
}

MoviesLayout.propTypes = {
  movies: PropTypes.element,
  pagination: PropTypes.element,
  filter: PropTypes.element
};