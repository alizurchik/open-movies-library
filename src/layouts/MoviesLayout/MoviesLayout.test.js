import React from 'react';
import ReactDOM from 'react-dom';
import MoviesLayout from './MoviesLayout';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MoviesLayout />, div);
});
