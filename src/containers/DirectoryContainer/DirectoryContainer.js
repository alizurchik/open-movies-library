import React from 'react';
import './DirectoryContainer.css';
import {Grid, Row, Col, Alert} from 'react-bootstrap';
import {ITEMS_PER_PAGE, omdbApi} from '../../services/omdb-api';
import {MoviesTable, SearchBar, MoviesFilter} from '../../components';
import {PaginatorContainer} from '../';
import MoviesLayout from '../../layouts/MoviesLayout/MoviesLayout';
import {pluck, unique} from '../../core/utils';

// helpers
const Movies = props => props.movies.length > 0 && <MoviesTable {...props} />;
const Warning = ({children}) => children && <Alert bsStyle="warning">{children}</Alert>;
//

export default class DirectoryContainer extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      movies: [],
      pages: null,
      query: '',
      years: [],
      types: []
    }
  }

  componentDidMount() {
    // focus input after it appears
    this.focusSearchBar();
  }

  render() {
    return (
      <Grid className="DirectoryContainer">
        <Row>
          <Col xs={10} xsOffset={1} md={6} mdOffset={3}>
            <SearchBar
              placeholder="Type movie name, e.g. Batman"
              debounce={500}
              onChange={this.handleSearchChange}
              inputRef={ref => this.searchBar = ref}
            />
          </Col>
        </Row>
        {this.state.query.length > 0 && <Warning>{this.state.error}</Warning>}
        <MoviesLayout
          filter={
            <MoviesFilter
              years={this.state.years}
              types={this.state.types}
              onSelectYear={this.handleYearFilter}
              onSelectType={this.handleTypeFilter}
            />
          }
          pagination={
            <PaginatorContainer
              page={this.state.page}
              pages={this.state.pages}
              onSelect={this.handlePageChange}
            />
          }
          movies={
            <Movies
              onClick={this.handleMovieClick}
              movies={this.state.movies}
            />
          }
        />
      </Grid>
    );
  }

  handleYearFilter = ({name}, selected) => {
    if (selected) {
      const options = {};

      if (name !== '_all') {
        options.year = name;
      }

      this.findMovie(this.state.query, options)
        .then(this.focusSearchBar); // unblock input
    }
  };

  handleTypeFilter = ({name}, selected) => {
    if (selected) {
      const options = {};

      if (name !== '_all') {
        options.type = name;
      }

      this.findMovie(this.state.query, options)
        .then(this.focusSearchBar); // unblock input
    }
  };

  handleMovieClick = movie => {
    this.props.history.push('/Details/' + movie.id);
  };

  handleSearchChange = val => {
    this.setState({
      // save current input in order to display warning later
      query: val
    }, () => {
      this.findMovie(val)
        .then(this.focusSearchBar); // unblock input
    });
  };

  handlePageChange = page => {
    this.setState({page});

    this.findMovie(this.state.query, {page})
      .then(this.focusSearchBar); // unblock input
  };

  focusSearchBar = () => {
    this.searchBar.focus();
  };

  findMovie(movieName, {page, year, type} = {}) {
    return omdbApi.search({query: movieName, page, year, type})
      .then(data => this.setState({
        error: null,
        movies: data.items,
        pages: Math.ceil(data.total / ITEMS_PER_PAGE),
        // extract unique years values and remap to shape {name: year}
        types: unique(pluck(data.items, 'type')).map(val => ({name: val})),
        // extract unique years values and remap to shape {name: type} and remove "ranged" years like 1991–1993, sort asc
        years: unique(pluck(data.items, 'year'))
          .filter(year => year.indexOf('–') === -1)
          .sort()
          .map(val => ({name: val}))
      }))
      .catch(err => this.setState({
        error: err.message,
        movies: [],
        pages: null,
        types: [],
        years: []
      }));
  }
}
