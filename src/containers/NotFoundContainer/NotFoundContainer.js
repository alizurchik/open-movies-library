import React from 'react';
import {NotFound} from '../../components';

export default class NotFoundContainer extends React.PureComponent {
  render() {
    return <NotFound homeUrl={process.env.REACT_APP_HOME_URL} />
  }
}
