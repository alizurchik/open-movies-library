import React from 'react';
import ReactDOM from 'react-dom';
import NotFoundContainer from './NotFoundContainer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NotFoundContainer />, div);
});
