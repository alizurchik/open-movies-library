import React from 'react';
import PropTypes from 'prop-types';
import {Paginator} from '../../components';

export default class PaginatorContainer extends React.Component {
  render() {
    if (!this.props.pages) {
      return null;
    }

    return <Paginator {...this.props} />
  }
}

PaginatorContainer.propTypes = {
  onSelect: PropTypes.func,
  pages: PropTypes.number,
  page: PropTypes.number
};

PaginatorContainer.defaultProps = {
  pages: 0,
  page: 1
};