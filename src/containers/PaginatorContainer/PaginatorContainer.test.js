import React from 'react';
import ReactDOM from 'react-dom';
import PaginatorContainer from './PaginatorContainer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PaginatorContainer />, div);
});
