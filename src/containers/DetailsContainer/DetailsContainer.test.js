import React from 'react';
import ReactDOM from 'react-dom';
import {MemoryRouter, withRouter} from 'react-router-dom'
import DetailsContainer from './DetailsContainer';

it('renders without crashing', () => {
  const div = document.createElement('div');

  const DetailsContainerWrapped = withRouter(DetailsContainer);
  ReactDOM.render(<MemoryRouter><DetailsContainerWrapped/></MemoryRouter>, div);
});
