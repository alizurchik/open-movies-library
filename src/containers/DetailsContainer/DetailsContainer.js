import React from 'react';
import {omdbApi} from '../../services/omdb-api';
import {MovieDetails} from '../../components';
import {Button, Glyphicon} from 'react-bootstrap';

export default class DetailsContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      movie: null
    };
  }

  render() {
    if (!this.state.movie) {
      return <h3>Loading...</h3>;
    }

    return (
      <section>
        <Button href="/"><Glyphicon glyph="chevron-left" /> Back</Button>
        <MovieDetails movie={this.state.movie} />
      </section>
    );
  }

  componentDidMount() {
    omdbApi.byId(this.props.match.params.id)
      .then(movie => this.setState({movie}))
      .catch(this.handleNotFound)
  }

  handleNotFound = () => {
    this.setState({movie: null});
    this.props.history.push('/NotFound');
  }
}