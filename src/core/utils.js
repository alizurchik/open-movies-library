/**
 * Checks if value is function
 * @param value
 * @returns {boolean}
 */
export function isFunction(value) {
  return typeof value === 'function';
}

/**
 * Make first letter capital
 * @param word
 * @returns {string}
 */
export function capitalizeWord(word) {
  return word.charAt(0).toUpperCase() + word.substr(1);
}

/**
 * Debounces function execution
 * @param fn
 * @param delay
 * @returns {function(...[*]=)}
 */
export function debounce(fn, delay) {
  let timeout;

  return (...args) => {
    clearTimeout(timeout);

    timeout = setTimeout(() => fn.apply(null, args), delay);
  }
}

/**
 * Returns unique values in array
 * @param arr
 */
export function unique(arr) {
  return arr.filter((v, i, a) => a.indexOf(v) === i);
}

/**
 * Plucks keys from items in array
 * @param arr
 * @param key
 */
export function pluck(arr, key) {
  return arr.map(item => item[key]);
}
