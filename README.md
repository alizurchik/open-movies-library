# Open Movies Library

## Features:

* The app is a directory of movies where you can search for specific movie name and get info about it.
* [omdbapi](http://www.omdbapi.com/) is used as an API provider (free).
* The app is a SPA bootstrapped with [`create-react-app`](https://github.com/facebookincubator/create-react-app).
* [Bootstrap](http://getbootstrap.com/) is used as a UX library
* [`react-router`](https://github.com/ReactTraining/react-router) handles location changes
* Available components are presented in [`styleguidist`](https://github.com/styleguidist/react-styleguidist) sandbox (setup described below).
* [`yarn`](https://github.com/yarnpkg/yarn) is supported
* No any of state managers (like `redux` or `flux`) were used because of simplicity of data flow

## Setup

Create `.env` file in root folder and set up variables:

* `REACT_APP_HOME_URL` - home url of web app (for dev it can be '/')

[More info on `.env` format](https://github.com/motdotla/dotenv)

Install dependencies:

`npm install`

Start local server:

`npm start`

### Optionally:

`npm run build` to build for production.
`npm run styleguide` to start sandbox server and see components library.
`npm test` to run tests (tests are written for all the components).

## Project Structure

All the code is placed in [`src`](/src) folder. There several folders with components:

* [`components`](/src/components) - presentational components
* [`containers`](/src/containers) - containers
* [`layouts`](/src/layouts) - [layouts](https://github.com/vasanthk/react-bits/blob/master/patterns/13.layout-container.md) for composing components

[`services`](/src/services) folder contains providers for making [remote](/src/services/request.js) requests and [API](/src/services/omdb-api.js) calls.

## Limitations & caveats

* No unit tests for [services](/src/services) and [utils](/src/utils)
* Lack of comments in code
* Only one filter can be applied at the time
* No constants for routes names
